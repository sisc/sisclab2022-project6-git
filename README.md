# sisclab2022-project6

## Documentation

**[SiScLab 2022, Project 6, Home](https://iffmd.fz-juelich.de/RhXA4J_JTwebjYKG35TbuQ)**.

## Installation

```shell
$ cd sisclab2022-project6-git
$ pip install -e .
```
