# main reason for this file: allow local dvelopment via "pip install -e ."
# reference:
# - google search > python import tricks
#   - https://levelup.gitconnected.com/import-your-own-python-code-without-pythonpath-tricks-9068495c1bba

from setuptools import setup, find_packages

requirements = [  # This could be retrieved from requirements.txt# Package (minimal) configuration
    'numpy',
    'matplotlib',
    'ase',
    'setuptools',
    'pandas',
    'scikit-learn'
]

setup(
    name='sisclab2022-project6',
    version='0.1.0',
    packages=find_packages(),  # __init__.py folders search
    url='https://git.rwth-aachen.de/sisc/sisclab2022-project6-git',
    license='MIT license ',
    author='Po-Yen Chen, Ilinca Burdulea, Lixia A, Johannes Wasmer',
    author_email='j.wasmer@fz-juelich.de',
    description='Tools for SiScLab 2022 Project 6 - A machine learning playground in quantum mechanical simulation',
    install_requires=requirements
)
