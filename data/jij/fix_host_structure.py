# Fix host crystal structure.
# The host crystal structure (a unit cell of Bi2Te3), from the source database (mozumders-dimer-database) had a little problem. 
# The last four atom positions were displaced by unit cell vectors -(a+b+c), compared to the first (a Te atom).
# This is only a problem for visualization of the unit cell. 
# It just looks as if the first atom is way off. Since it is periodic, it was still a valid unit cell.
# To fix it, we could either add (a+b+c) to the last four atoms.
# So, we load the structure from file, do the fix, and save it again.
import ase.io

host_structure = ase.io.read("host_structure_Bi2Te3.extxyz", format='extxyz')

# check first if the fix has already been applied.
z_positions = host_structure.get_positions()[:,2]
unfixed_z_positions = np.array([ 0., 26.78792133, 24.02376556, 22.35714223, 28.45454466])
if not np.allclose(z_positions, unfixed_z_positions):
    raise ValueError("Fix host crystal structure has already been applied.") 

# get correct position
pos0 = host_structure.get_positions()[0]
# get positions to shift
other_pos = host_structure.get_positions()[1:]
# get unit cell vectors
a,b,c = host_structure.cell
# shift positions
other_pos -= (a+b+c)
# define new positions
newpos = np.vstack((pos0, other_pos))
# set new positions
host_structure.set_positions(newpositions=newpos)
# center
host_structure.center()
# save structure
ase.io.write("host_structure_Bi2Te3_fixed.extxyz", images=host_structure, format='extxyz')
# Done with fix.