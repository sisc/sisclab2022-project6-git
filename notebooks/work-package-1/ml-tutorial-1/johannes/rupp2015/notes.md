


## Notes on Rinke2019

Notes on the paper [A. Stuke, et al. "Chemical diversity in molecular orbital energy predictions with kernel ridge regression." J. Chem. Phys. 150. 204121 (2019)](https://aip.scitation.org/doi/10.1063/1.5086105) mentioned by Po-Yen in his `task_Rupp2015` notebook.

Model KRR, Descriptors CM MBTR.

Input molecules from three datasets.

- QM9: 134k small organic compounds
- AA: 44k amino acids and dipeptides
- OE: 64k opto-electronically active molecules

Prediction of "highest occupied molecular orbital (HOMO) energies, computed at the density-functional level of theory".

-----

- For CM+KRR, use Laplacian kernel. 

  > The Laplacian kernel, on the other hand, can better model piecewise smooth functions, such as discontinuities of the sorted CM.
  
- For MBTR+KRR, use Gaussian kernel.

  > The Gaussian kernel performs best on the MBTR, whose N-body terms themselves consist of Gaussian distributions.
  
-----

Benchmark study design

TODO formulate as recipe.