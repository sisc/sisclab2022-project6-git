"""Visualization tools for feature engineering."""

import abc as _abc
import copy as _copy
import dataclasses as _dc
import typing as _typing

import ase as _ase
import ase_notebook as _ase_nb
import dscribe.descriptors as _descriptors
import matplotlib as _mpl
import matplotlib.pyplot as _plt
import numpy as _np
import sklearn.preprocessing
from matplotlib import pyplot as _plt
from matplotlib.collections import QuadMesh as _mpl_QuadMesh


@_dc.dataclass(init=True, repr=True, eq=True, order=False)
class PlotterSettings:
    figsize: tuple = (12, 5)
    fontsize: int = 14
    cmap: str = 'viridis'


class Plotter(_abc.ABC):
    def __init__(self,
                 settings: PlotterSettings = PlotterSettings()):
        self.settings = settings

    def _get_fig_ax(self,
                    fig: _plt.Figure = None,
                    ax: _plt.Axes = None) -> _typing.Tuple[_plt.Figure, _plt.Axes]:
        s = self.settings
        if fig is None and ax is None:
            fig, ax = _plt.subplots(figsize=s.figsize)
        elif fig is not None and ax is None:
            ax = _plt.gca()
        return fig, ax


class StructurePlotter(Plotter):
    @staticmethod
    def view(atoms: _ase.Atoms) -> _ase_nb.backend.threejs.RenderContainer:
        config = _ase_nb.ViewConfig()
        ase_view = _ase_nb.AseView(config)
        try:
            return ase_view.make_render(atoms, center_in_uc=True)
        except IndexError as err:
            print("Could not render atoms: IndexError.")


class FeatureVectorPlotter(Plotter):
    """"Creates a 1D mpl figure of the feature vector(s) produced by a DScribe descriptor.

    References:
        - for the matplotlib class design: maticalderini's notebook tutorials
        https://github.com/maticalderini/Tutorial_pltOrganization,
        https://github.com/maticalderini/tutorial_matplotlibCustomPlots
    """

    def __init__(self,
                 descriptor_type: _abc.ABCMeta == _descriptors.SOAP,
                 settings: PlotterSettings = PlotterSettings()):
        super().__init__(settings)
        assert issubclass(descriptor_type, _descriptors.Descriptor)
        self.descriptor_type = descriptor_type

    def lineplot(self, feature_vector: _np.ndarray,
                 symbols: _typing.List[str],
                 fig: _plt.Figure = None,
                 ax: _plt.Axes = None,
                 **kwargs) -> _typing.Tuple[_plt.Figure, _plt.Axes]:

        s = self.settings
        fig, ax = self._get_fig_ax(fig, ax)

        _fingerprint = feature_vector.copy()
        _symbols = _copy.copy(symbols)

        # if the fingerprint has shape (N,) then convert to (1,N) row vector.
        if len(_fingerprint.shape) == 1:
            _fingerprint = _fingerprint[None, :]

        x_offset = kwargs['x_offset'] if kwargs.get('x_offset', None) else 0
        x = _np.linspace(x_offset, x_offset + _fingerprint.shape[1], num=_fingerprint.shape[1], dtype=int)

        # remove custom kwargs
        kwargs_inner = _copy.deepcopy(kwargs)
        if kwargs_inner.get('x_offset', None):
            kwargs_inner.pop('x_offset')

        # print fingerprint for each element
        for i in range(_fingerprint.shape[0]):
            ax.plot(x, _fingerprint[i, :], label=_symbols[i], **kwargs_inner)

        return fig, ax

    def heatmap(self, feature_vector: _np.ndarray,
                symbols: _typing.List[str],
                fig: _plt.Figure = None,
                ax: _plt.Axes = None,
                **kwargs) -> _typing.Tuple[_plt.Figure, _plt.Axes, _mpl_QuadMesh]:

        s = self.settings
        fig, ax = self._get_fig_ax(fig, ax)

        _fingerprint = feature_vector.copy()
        _symbols = _copy.copy(symbols)

        # if the fingerprint has shape (N,) then convert to (1,N) row vector.
        if len(_fingerprint.shape) == 1:
            _fingerprint = _fingerprint[None, :]

        # if the image to be plotted has shape (1, N), then pcolormesh would plot a white canvas.
        # Dirty hack: duplicate to (2,N) and plot that instead.
        if _fingerprint.shape[0] == 1:
            _fingerprint = _np.vstack((_fingerprint, _fingerprint))
            _symbols = symbols + ['']

        x_offset = kwargs['x_offset'] if kwargs.get('x_offset', None) else 0
        x = _np.linspace(x_offset, x_offset + _fingerprint.shape[1], num=_fingerprint.shape[1], dtype=int)
        y = _np.linspace(0, _fingerprint.shape[0], num=_fingerprint.shape[0], dtype=int)

        # we want the symbols as y axis ticks. only doing this:
        # >>> ax.set_yticklabels(symbols)
        # would trigger the warning 'FixedFormatter should only be used together with FixedLocator'.
        # Reason in docstring: This method should only be used after fixing the tick positions using `.Axes.set_yticks`.
        # In order to set the labels, first set the ticks. Reference: https://stackoverflow.com/a/63755285/8116031
        ax.set_yticks(y)
        ax.set_yticklabels(_symbols)

        # remove custom kwargs
        kwargs_inner = _copy.deepcopy(kwargs)
        if kwargs_inner.get('x_offset', None):
            kwargs_inner.pop('x_offset')

        # pcolormesh produces nicer format than matshow
        pcm = ax.pcolormesh(x, y, _fingerprint, shading='auto', cmap=s.cmap, **kwargs_inner)
        # cax = ax.matshow(fingerprint, aspect='auto')

        # fig.colorbar(mappable, ax=list)
        return fig, ax, pcm

    def combined_lineplot_heatmap(self,
                                  fingerprint: _np.ndarray,
                                  symbols: _typing.List[str],
                                  **kwargs):
        s = self.settings

        ################
        # prepare canvas
        fig = _plt.figure(figsize=s.figsize)
        gridspec = _mpl.gridspec.GridSpec(nrows=2,
                                          ncols=2,
                                          width_ratios=[18, 1],
                                          height_ratios=[1, 1],
                                          wspace=0.05,
                                          hspace=0.3)
        ax0 = fig.add_subplot(gridspec[0, 0])  # for lineplot
        ax1 = fig.add_subplot(gridspec[1, 0])  # for heatmap
        ax3 = fig.add_subplot(gridspec[1, 1])  # for colorbar of heatmap

        #############
        # fill canvas
        # lineplot
        ax0.cla()
        self.lineplot(feature_vector=fingerprint, symbols=symbols, fig=fig, ax=ax0, **kwargs)
        # put the lineplot legend right to the plot, same position and size of the heatmap's colorbar below
        ax0.legend(fontsize=10, bbox_to_anchor=(1.025, 1.0), ncol=1, borderaxespad=0, frameon=True)
        ax0.set_xbound(lower=0, upper=300)

        # heatmap
        ax1.cla()
        fig, ax, pcm = self.heatmap(feature_vector=fingerprint, symbols=symbols, fig=fig, ax=ax1, **kwargs)

        # heatmap colorbar
        fig.colorbar(pcm, cax=ax3)

        # # ensure no overlap of labels between axes
        # # # but here, i want legend to be aligned to colorbar axes, so NO tight layout
        # fig.tight_layout()

        # align axeses axis ticks
        ax0.set_xbound(ax1.get_xbound())
        # ax1.xaxis.set_visible(False)

        ax1.set_xlabel(f'{self.descriptor_type.__name__} value')

        return fig, _np.array([ax0, ax1, ax3])


def plot_feature_matrix(X: _np.ndarray,
                        figsize: tuple = (10, 6),
                        transpose: bool = True,
                        cmap: str = "viridis",
                        with_colorbar: bool = False,
                        colorbar_pad: float = None,
                        filepath: str = None) -> None:
    """Plot feature matrix as image min-max-scaled (all features in [0,1]).
    
    If saving via function makes trouble, call plt.savefig() immediately afterwards instead.
    
    To crop figure to content, use e.g. plt.savefig(filepath, transparent=True, bbox_inches='tight', pad_inches=0).

    :param X: feature matrix.
    :param figsize: figure size.
    :param transpose: transpose image. Normally True, since more samples than features.
    :param cmap: mpl colormap.
    :param with_colorbar: Add colorbar to plot.
    :param colorbar_pad: float, default: 0.05. Fraction of original axes between colorbar and new image axes.
    :param filepath: if not None, call `savefig(filepath)` on plot (use "...png" or ".pdf").
    """
    min_max_scaler = sklearn.preprocessing.MinMaxScaler()
    X_scaled = min_max_scaler.fit_transform(X)
    X_scaled = X_scaled.transpose() if transpose else X_scaled

    # print(f"{x.mean()=}, {x.std()=}, {x.min()=}, {x.max()=}")
    # print(f"{x_scaled.mean()=}, {x_scaled.std()=}, {x_scaled.min()=}, {x_scaled.max()=}")

    assert(isinstance(X_scaled, _np.ndarray))
    is_vertical = X_scaled.shape[0] >= X_scaled.shape[1]

    fig = _plt.figure(figsize=figsize)
    _plt.imshow(X_scaled, cmap=cmap, interpolation='nearest')

    if with_colorbar:
        colorbar_pad = colorbar_pad if colorbar_pad else 0.05
        fig.colorbar(pad=colorbar_pad).ax.tick_params(labelsize=20)

    _plt.axis('off')
    fig.tight_layout()
    # _plt.show()

    if filepath:
        fig.savefig(filepath)
        print(f"Saved plot to '{filepath}'.")
        
    return fig
