"""Stuff for plotting ML target properties (or other non-featurized data.)"""

import typing
import random

import numpy as np
import pandas as pd
import sklearn.model_selection
import matplotlib.pyplot as plt
import seaborn as sns

import sisclab22p6.plotting


def create_heatmap_matrix(table: pd.DataFrame,
                          value_column: str = 'J',
                          ids_column: str = 'jij_uuid',
                          value_array: np.ndarray = None,
                          ids_array: np.ndarray = None,
                          ilayer1: int = 3,
                          offset1: int = 1,
                          aggregate_function: str = None,
                          zero_out_threshold: float = 0.01,
                          nan_value=np.nan) -> typing.Optional[typing.Tuple[pd.DataFrame, typing.Set[str]]]:
    """Create a 'heatmap matrix' dataframe for plotting with rows= defect atom 0 symbols, columns = defect atom 1 symbols.

    This is for a dataset of double impurity embeddings (= two defect atoms) into a host crystal. A table is expected with at least the
    column names `imp0` (atomic number of defect atom 0), `imp1` (atomic number of defect atom 1), `ilayer1`, `offset1`
    (see params), and another column with values to put in the heatmap. I expect that each defect atom pair (`imp0`, `imp1`) is present in different configurations
    (= multiple rows), where `ilayer1` and `offset1` varies. If values for ilayer1, offset1 are passed to me, I will only put the values (=cell color) of those structures (rows) into
    the heatmap, for each cell (=defect pair) in the heatmap. If ilayer1, offset1 are not specified, then I require an aggregate function, which I will apply to
    the values of all ilayer1, offset1 configurations for each defect pair. Then I put this aggregate value into the respective defect pair's cell in the heatmap.

    Get valid ilayer1, offset1 options from table with `table['ilayer1'].unique(), table['offset1'].unique()`.

    Usage examples: data exploration; parity plot heatmap (plot difference predicted vs. true value from supervised learning of a target property).

    >>> # plot Jij heatmap (data exploration)
    >>> heatmap_matrix, missing_defect_pairs = create_heatmap_matrix(table, value_column='J')
    >>>
    >>> # plot Jij prediction from ML model, test set only
    >>> # need additional array of row IDs for tracking the samples during ML training, to find their correct rows again afterwards
    >>> y = table['J'].values
    >>> ids = list(table['jij_uuid']) # unique sample identifiers undergo same split & shuffle as X,y
    >>> X = descriptor.transform(table['structure'])
    >>> X_train, X_test, y_train, y_test, ids_train, ids_test = sklearn.model_selection.train_test_split(X, y, ids, test_size=0.25)
    >>> model.fit(X_train, y_train)
    >>> y_test_pred = model.predict(X_test)
    >>> # function create_heatmap_matrix uses the ids now to re-match the split & shuffled y's back to their correct table rows = structures
    >>> heatmap_matrix, missing_defect_pairs = create_heatmap_matrix(table, value_array=y_test_pred, ids_array=ids_test)
    >>>
    >>> # plot Jij "parity heatmap" (element-wise MAE), test set only
    >>> # Mean absolute errror, MAE := sum(abs(y - ypred))/len(y).
    >>> y_test_abs_err = abs(y_test - y_pred_test)
    >>> heatmap_matrix_parity, missing_defect_pairs = create_heatmap_matrix(table, value_array=y_test_abs_err, ids_array=ids_test)
    >>>  
    >>> # plot Jij "parity heatmap", on all data (X has same row order as table -> no ID re-matching needed!)
    >>> y_pred = model.predict(X)
    >>> y_abs_err = abs(y - y_pred)
    >>> heatmap_matrix_parity, missing_defect_pairs = create_heatmap_matrix(table, value_array=y_abs_err)
    >>>
    >>> # plot heatmap and save
    >>> plot_heatmap(heatmap_matrix_parity, publication_target='slides', fontsize='small')
    >>> plt.savefig('fig/heatmap-jij-parity-plot-abs-err.pdf')

    :param table: Table with columns for unique row ID, optionally `value_column`, `ilayer1`, `offset1`.
    :param value_column: Column name with which to fill the heatmap. Ignored if `value_array` given.
    :param ids_column: Only needed if `value_array` given. To match table row IDs with `ids_array`.
    :param value_array: If given, use these values for the heatmap instead of the ones in column `value_column` column from table. If same length as table and `valude_ids` not given, append to table without ID matching.
    :param ids_array: Only needed if `value_array` given and not same length as table. If given, match values in array to correct rows in table via their matching IDs.
    :param ilayer1: Bi layer of defect atom 1 inside the Bi2Te3 unit cell (expecting this name in table). If `ilayer1` and `offset1` both None, use `aggregate_function` instead.
    :param offset1: unit cell offset of defect atom 1 to d.f. 0 (expecting this name in table). If `ilayer1` and `offset1` both None, use `aggregate_function` instead.
    :param aggregate_function: Aggregate multiple values for same defect atom pair with one of functions: 'mean', 'median', 'sum', 'min', 'max', 'count'. Ignored if `ilayer1` and `offset1` are both not None.
    :param zero_out_threshold: Set values to zero if absval < threshold. Deactivate with threshold=0 or None.
    :param nan_value: Value to use if df holds no value for that combination. Options: np.nan, pd.NA, None, 0.0, ...
    :return: tuple of heatmap matrix (DataFrame for plotting), 'empties' (set of defect pairs for which no values were present). Or None if something goes wrong.

    DEVNOTES:

    - replace selection hard-coded to columns (ilayer1, offset1) to generic select_by : Dict[column_name : List (of values to include)] param. Then agg_fun arg is moot if only one value included.
      (On the other hand, a savvy pandas user could pre-filter table to their liking on their own ... for instance, this approach would not allow to specify value ranges ...)
    """
    # variable naming: 'defect atom' = 'impurity' = imp
    imp0_numbers_column = 'imp0'
    imp1_numbers_column = 'imp1'
    imp1_ilayer_column = 'ilayer1'
    imp1_offset_column = 'offset1'

    # if value_array given, merge with table by ids_array, and choose the new column as value column
    # '_name' variables: internal copies of passed in parameters
    _table = table.copy()
    _value_column = value_column
    if value_array is not None:
        # create random column names (hash string)
        _value_column = f"{random.getrandbits(128):032x}"
        if ids_array is None:
            # assume value array same length as table and just append
            _table[_value_column] = value_array
        else:
            # merge value column into table by matching IDs
            new_ids_column = f"{random.getrandbits(128):032x}"
            new_values_with_ids = pd.DataFrame({_value_column: value_array, new_ids_column: ids_array})
            _table = pd.merge(_table, new_values_with_ids, left_on=ids_column, right_on=new_ids_column,
                              how='left').drop(new_ids_column, axis=1)

    required_cols = [imp0_numbers_column, imp1_numbers_column, imp1_ilayer_column, imp1_offset_column, _value_column]
    if not all(col in _table.columns for col in required_cols):
        raise ValueError("Passed dataframe does not contain all required columns {required_cols}.")

    agg_fun = {
        'mean': np.mean,
        'median': np.median,
        'sum': np.sum,
        'min': np.min,
        'max': np.max,
        'count': np.count_nonzero
    }
    use_agg_fun = (ilayer1 is None) or (offset1 is None)
    if use_agg_fun and aggregate_function not in agg_fun.keys():
        raise ValueError(
            f"Unknown aggregate function '{aggregate_function}'. Valid options are: {list(agg_fun.keys())}.")
    if not use_agg_fun:
        # check that ilayer1, offset1 choice is valid
        valid_ilayer1_values = _table[imp1_ilayer_column].unique()
        valid_offset1_values = _table[imp1_offset_column].unique()
        if not (ilayer1 in valid_ilayer1_values and offset1 in valid_offset1_values):
            raise ValueError(
                f"{ilayer1=} or {offset1=} not in valid table's values {valid_ilayer1_values=}, {valid_offset1_values=}.")
    if not use_agg_fun and aggregate_function is not None:
        print(
            "Warning: Both (ilayer1, offset1) given, and aggregate_function given. I will collect single values for the former and ignore the latter.")

    # host_symbols = ChemicalElements(elements=df['host_symbol'].unique().tolist())
    # imp_symbols = ChemicalElements(elements=df['imp_symbol'].unique().tolist(), special_elements={'X':0})

    # get all defect atom pairs to define the heatmap matrix, then turn all atomic numbers into chemical element symbols
    pte = {0: 'X', 1: 'H', 2: 'He', 3: 'Li', 4: 'Be', 5: 'B', 6: 'C', 7: 'N', 8: 'O', 9: 'F', 10: 'Ne',
           11: 'Na', 12: 'Mg', 13: 'Al', 14: 'Si', 15: 'P', 16: 'S', 17: 'Cl', 18: 'Ar',
           19: 'K', 20: 'Ca', 21: 'Sc', 22: 'Ti', 23: 'V', 24: 'Cr', 25: 'Mn', 26: 'Fe', 27: 'Co', 28: 'Ni', 29: 'Cu',
           30: 'Zn',
           31: 'Ga', 32: 'Ge', 33: 'As', 34: 'Se', 35: 'Br', 36: 'Kr',
           37: 'Rb', 38: 'Sr', 39: 'Y', 40: 'Zr', 41: 'Nb', 42: 'Mo', 43: 'Tc', 44: 'Ru', 45: 'Rh', 46: 'Pd', 47: 'Ag',
           48: 'Cd',
           49: 'In', 50: 'Sn', 51: 'Sb', 52: 'Te', 53: 'I', 54: 'Xe',
           55: 'Cs', 56: 'Ba',
           57: 'La', 58: 'Ce', 59: 'Pr', 60: 'Nd', 61: 'Pm', 62: 'Sm', 63: 'Eu', 64: 'Gd', 65: 'Tb', 66: 'Dy', 67: 'Ho',
           68: 'Er', 69: 'Tm', 70: 'Yb',
           71: 'Lu', 72: 'Hf', 73: 'Ta', 74: 'W', 75: 'Re', 76: 'Os', 77: 'Ir', 78: 'Pt', 79: 'Au', 80: 'Hg', 81: 'Tl',
           82: 'Pb', 83: 'Bi', 84: 'Po', 85: 'At', 86: 'Rn',
           87: 'Fr', 88: 'Ra',
           89: 'Ac', 90: 'Th', 91: 'Pa', 92: 'U', 93: 'Np', 94: 'Pu', 95: 'Am', 96: 'Cm', 97: 'Bk', 98: 'Cf', 99: 'Es',
           100: 'Fm', 101: 'Md', 102: 'No',
           103: 'Lr', 104: 'Rf', 105: 'Db', 106: 'Sg', 107: 'Bh', 108: 'Hs', 109: 'Mt', 110: 'Ds', 111: 'Rg', 112: 'Cn',
           113: 'Nh', 114: 'Fl', 115: 'Mc', 116: 'Lv', 117: 'Ts', 118: 'Og'}
    imp0_numbers = sorted(_table[imp0_numbers_column].unique())
    imp1_numbers = sorted(_table[imp1_numbers_column].unique())
    imp0_dict = {atomic_number: pte[atomic_number] for atomic_number in imp0_numbers}
    imp1_dict = {atomic_number: pte[atomic_number] for atomic_number in imp0_numbers}

    # create zero-'matrix' from host elements, impurity element labels
    matrix = pd.DataFrame(index=imp0_dict.values(), columns=imp1_dict.values(), dtype=int)
    matrix = matrix.fillna(nan_value)

    # accumulate values in intermediate container
    tmp = {imp0_num: {imp1_num: [] for imp1_num in imp1_dict} for imp0_num in imp0_dict}

    def _collect(imp0_num, imp1_num, current_ilayer1, current_offset1, value):
        if use_agg_fun or ((current_ilayer1 == ilayer1) and (current_offset1 == offset1)):
            tmp[imp0_num][imp1_num].append(value)

    # iterate over dataframe with list comprehension (fastest way apart from vectorization)
    # DEVNOTE: as said docstring TODO: hard-coding to ilayer1, offset1 columns looks stupid ... instead use pandas group/select by...
    [_collect(imp0_num, imp1_num, current_ilayer1, current_offset1, value) \
     for imp0_num, imp1_num, current_ilayer1, current_offset1, value \
     in zip(_table[imp0_numbers_column], _table[imp1_numbers_column], \
            _table[imp1_ilayer_column], _table[imp1_offset_column], \
            _table[_value_column])]

    # fill matrix by applying aggregate function
    missing_defect_pairs = set()
    for imp0_num, imp1s in tmp.items():
        for imp1_num, values in imp1s.items():
            if not values:
                imp_num_pair = (imp0_num, imp1_num)
                if not imp_num_pair in missing_defect_pairs:
                    missing_defect_pairs.add(imp_num_pair)
            else:
                cell_value = agg_fun[aggregate_function](values) if use_agg_fun else values[0]
                if pd.notna(cell_value) and zero_out_threshold:
                    cell_value = cell_value if abs(cell_value) >= zero_out_threshold else 0
                matrix.at[imp0_dict[imp0_num], imp1_dict[imp1_num]] = cell_value

    return matrix, missing_defect_pairs


def plot_heatmap(heatmap_matrix: pd.DataFrame,
                 absval: bool = False,
                 logscale: bool = False,
                 fontsize: str = 'small',
                 publication_target: str = 'slides',
                 cmap: str = 'RdBu_r',
                 center: float = 0,
                 annot: bool = True,
                 fmt: str = ".2g",
                 **sns_kwargs):
    """Plot a heatmap matrix created by function `create_heatmap_matrix`.

    Colormap `cmap`: Choose from https://seaborn.pydata.org/tutorial/color_palettes.html or https://matplotlib.org/stable/tutorials/colors/colormaps.html.

    To reverse direction of any cmap, add suffix '_r' to its name.

    Annotation formats: See https://stackoverflow.com/a/65020192/8116031.

    To reproduce figures from Rubel Mozumder's master thesis:

    - Figure 22 (Jij values): create_heatmap_matrix(ilayer1=3, offset1=1), plot_heatmap(cmap='RdBu_r').

    :param heatmap_matrix: heatmap DataFrame created by function `create_heatmap_matrix`.
    :param absval: Convert cell values to absolute values for plotting.
    :param logscale: Plot cell values in logarithmic scale.
    :param fontsize: See function `set_plt_fontsizes`.
    :param publication_target: See function `set_plt_fontsizes`.
    :param cmap: seaborn color palette or matplotlib colormap.
    :param center: Center colormap on this value.
    :param annot: If True, print the data value string also into each cell.
    :param fmt: String formatting code to use when adding annotations. Default: ".2g" = two significant figures.
    :param sns_kwargs: optional seaborn `heatmap` arguments.
    """
    if absval or logscale:
        heatmap_matrix = heatmap_matrix.applymap(lambda x: abs(x))

    # use same font for targets paper and slide: 12
    sisclab22p6.plotting.set_plt_fontsizes(fontsize=fontsize, publication_target=publication_target)

    plt.close('all')
    fig = plt.figure(figsize=(12, 8))

    if not logscale:  # =linscale
        sns.heatmap(heatmap_matrix, xticklabels=1, yticklabels=1, cmap=cmap, center=center, annot=annot, fmt=fmt,
                    robust=True, **sns_kwargs)
    else:
        from matplotlib.colors import LogNorm
        sns.heatmap(heatmap_matrix, xticklabels=1, yticklabels=1, cmap=cmap, center=center, annot=annot, fmt=fmt,
                    norm=LogNorm(), **sns_kwargs)

    plt.tick_params(axis='x', which='major', labeltop=True, labelrotation=90)
    plt.tick_params(axis='y', which='major', labelright=True, labelrotation=0)
    plt.tight_layout()

    # return fig
