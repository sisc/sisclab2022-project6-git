"""Tools for feature engineering."""

import typing as _typing

import ase as _ase
import numpy as _np
import numpy as np


def get_chemical_species(structures: _typing.List[_ase.Atoms]) -> _typing.Set[str]:
    """ Get chemical species present in the input structures.

    Example use case: Get all chemical elements present in a dataset for featurization as SOAP feature vectors.

    :param structures: list of atomic structures.
    :return: set of elements (chemical symbols).
    """
    #
    return set(_np.concatenate([_np.unique(struc.numbers) for struc in structures]))


def get_defect_atoms_indices(structures: _typing.List[_ase.Atoms]) -> _typing.List[int]:
    """ Get indices in ase atoms array of defect atoms in a host crystal.

    For now, finds defect atoms by checking for atomic number < 52. This works for example for host crystal
    Bi2Te3 with transition metal defects (Te = 52, Bi = 83, transition metals < 52).

    Example use case: Need to get positions of defect atoms to featurize their local atomic environment.

    :param structures: list of atomic structures.
    :return: list of indices of the defect atoms.
    """
    # assumption: all defect atoms are transition `metals and have atomic numbers < host crystal atomic numbers (Bi: 83, Te: 52)
    defect_atoms_indices = [list(_np.asarray(struc.numbers < 52).nonzero()[0]) for struc in structures]
    # check that all index pairs are equal, if so, flatten the list
    if not all(index_pair == defect_atoms_indices[0] for index_pair in defect_atoms_indices):
        raise Exception("Not all defect atoms indices equal. Have to do by-case featurization.")
    return defect_atoms_indices[0]


def get_defect_atoms_positions(structures: _typing.List[_ase.Atoms]) -> _typing.List[_typing.List[_np.ndarray]]:
    """ Get Euclidean coordinates of the defect atoms.

    Example use case: Need to get positions of defect atoms to featurize their local atomic environment.

    :param structures: list of atomic structures.
    :return: list (one item per structure) of list (one array per defect atom) of defect atom coordinates
    """
    defect_atoms_indices = get_defect_atoms_indices(structures)
    return [[struc.positions[idx] for idx in defect_atoms_indices] for struc in structures]
    # DScribe SOAP needs positions per structure. So do not flatten the list, as we did with indices.


def get_atom_distance_statistics(structures: _typing.List[_ase.Atoms],
                                 atom_idx: int = 0,
                                 verbose: bool = True) -> _typing.Dict[int, _typing.Dict[str, np.ndarray]]:
    """Get statistics on atoms distances in structures.

    Collects statistics on all distances as measured from atom at specified index.

    In mozumder's dimer impurities dataset, atom 0 is defect atom 0 in all structures.

    :param structures: list of atomic structures.
    :param atom_idx: Atom from which to measure distances to the other atoms.
    :param verbose: Also print statistics nicely formatted to system out.
    :return: Dict {n_atoms : {statistic_name : array}}, where array holds position statistic for each other atom in
    structure.
    """
    # #
    # # Get info on atom distances
    # #
    # get distinct number of atoms from all structures
    n_atoms_set = set(struc.get_global_number_of_atoms() for struc in structures)

    # get distances to atom 0 for all structures
    distances_all_structures = [(struc.get_distances(a=0, indices=[idx for idx in range(n_atoms) if idx != atom_idx]))
                                for struc, n_atoms \
                                in zip(structures, [s.get_global_number_of_atoms() for s in structures])]

    # structures could have different numbers of atoms
    # So, split list of distances by number of atoms, turn into numpy arrays for doing statistics
    # n_atoms = n_distances + 1, cause we collected distances to atom 0 above
    all_distances_by_n_atoms = {n_atoms: np.array([d for d in distances_all_structures if len(d) == (n_atoms - 1)]) \
                                for n_atoms in n_atoms_set}

    # print atom distances statistics via aggregation functions
    agg_funs = {
        "count": np.count_nonzero,
        "min": np.min,
        "max": np.max,
        "mean": np.mean,
    }

    # pack into {n_atoms : {statistic_name : distance statistics}}
    distance_statistics = {n_atoms: {agg_name: agg_fun(distances, axis=0) \
                                     for agg_name, agg_fun in agg_funs.items()} \
                           for n_atoms, distances in all_distances_by_n_atoms.items()}
    # reduce 'count' array to only one entry (instead of repeated, identical count for each other atom)
    for n_atoms, named_stats in distance_statistics.items():
        distance_statistics[n_atoms]["count"] = named_stats["count"][0]

    if verbose:
        _np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
        print(f"Atom distance statistics, measured from atom at index {atom_idx}.")
        for n_atoms, named_stats in distance_statistics.items():
            print(f"For the structures with {n_atoms=}:")
            for name, stat in named_stats.items():
                print(f"{name} distances to atom 0: {stat}")

    return distance_statistics
