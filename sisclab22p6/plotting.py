"""General plotting functionalities."""

import matplotlib.pyplot as _plt
import typing as _typing


def set_plt_fontsizes(fontsize: _typing.Union[str, int] = 'small', publication_target: str = 'slides'):
    """Reset plt (matplotlib.pyplot) rcParams font sizes.

    Resets title, legend, axes labels, axes ticks font to same size.

    :param fontsize: either 'small', 'medium', 'big', or a specific integer.
    :param publication_target: either 'report' or 'slides'. 'slides' uses larger font sizes.
    """
    sizes = {
        'small': 12,
        'medium': 16,
        'big': 20
    }
    if publication_target == 'slides':
        sizes = {
            'small': 16,
            'medium': 20,
            'big': 24
        }
    fs = sizes[fontsize] if isinstance(fontsize, str) else fontsize
    params = {'axes.labelsize': fs,
              'axes.titlesize': fs,
              'xtick.labelsize': fs,
              'ytick.labelsize': fs,
              'legend.fontsize': fs}
    _plt.rcParams.update(params)
