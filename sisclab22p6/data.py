"""Tools for loading and prreprocessing input data."""

import re as _re
import typing
import typing as _typing
from pathlib import Path as _Path

import ase as _ase
import ase.io as _ase_io
import pandas as _pd


def _process_rcut_columns(table: _pd.DataFrame,
                          rcut_column_names: _typing.List[str]) -> typing.List[str]:
    """Private helper function of `load_jij_data`. Match Rcut columns to impurity numbering of table.

    Finds out which Rcut column belongs to 'imp0' and which to 'imp1', renames them accordingly. Table operations are
    in-place.

    Each structure is a cut-out of a small region inside the host crystal. This region consists of several
    single-impurity "clusters". These clusters consist of a small sphere of host crystals around the defect
    atom = impurity embedding in the center of the cluster. The radius of this cut-out sphere is determined
    by a parameter called 'Rcut' in the original AiiDA-KKR calculation. There is one such Rcut parameter for  each
    impurity cluster inside the structure.

    In the structure's info dict, there are entries `Rcut_at_site_X`, one for each defect atom in the structure.
    'X' is a placeholder for the index of the respective impurity atom in the cluster structure. We do not know
    beforehand, what values 'X' takes, so there may be several different KKR Rcut entries in the info dict with
    different 'X'. For the merged table, we need to find out which entry belongs to which impurity atom, so that it
    fits to impurity pair indexing '0' and '1' used in the columns from the original .csv table.

    How do we do that? From the table, we find out the Z0 and Z1 (chemical species) of imp0 and imp1 in the table.
    Then from the structure we find out the Z's of the atoms sitting at index specified by `Rcut_at_site_X`. With
    that information, we can relabel the Rcut entries to `rcut0` and `rcut1`.

    :param table: intermediate table in `load_jij_data`.
    :param rcut_column_names: names of KKR Rcut columns, except the Rcut unit column.

    :return List of renamed Rcut column names, INCLUDING the Rcut unit column.
    """
    # get defect atom indices in structure
    imp_indices = [[int(s) for s in _re.findall(r'\d+', col_name)][0] for col_name in rcut_column_names]

    if len(imp_indices) != 2:
        print(f"Warning: Expected two find two defect atom indices from 'Rcut' columns, but found {imp_indices}.")
        if len(imp_indices) > 2:
            print(f"Warning: Will ignore defect atom indices {imp_indices[2:]}.")

    # get Z (chemical species) of defect atoms from structures via imp_indices

    jij_uuid_column_name = 'jij_uuid'

    imp_atomic_numbers_structure = _pd.DataFrame(
        data=[[uuid] + list(structure.get_atomic_numbers()[imp_indices]) for uuid, structure in
              zip(table[jij_uuid_column_name], table['structure'])],
        columns=[jij_uuid_column_name, 'imp0_structure', 'imp1_structure'])

    # pd.DataFrame(imp_atomic_numbers_structure, columns=['Z0_structure', 'Z1_structure'])

    Z_table = _pd.merge(left=table[[jij_uuid_column_name, 'imp0', 'imp1']], right=imp_atomic_numbers_structure,
                        left_on=jij_uuid_column_name, right_on=jij_uuid_column_name,
                        how='left')

    # Lazy approach: check if columns are equal. If so, can just relabel the Rcut columns accordingly.
    #
    # General approach: Not implemented. If columns not equal, fail with error message.
    #
    # General approach would be to row-wise compare imp0, imp1 and assign Rcut columns accordingly.
    # But should that become necessary, a better way is to abandon this whole 'merge Rubel's csv table with list of structures'
    # entirely, and instead use aiida-jutools NodeTabulator to create one table directly from the source AiiDA data, with both
    # the info from Rubel's table and the structures already included. Then such matching becomes unnecessary, and handling of
    # more complex cases (more than two defect atoms, defect atoms at different positions in structure) becomes more easy.
    #
    # (There is an edge case which would not require to switch to NodeTabulator just yet: If a subset A of imp0 match with imp0_structure
    # and the remainder subset B with imp1_structure, and vice versa subset A of imp1 matches with imp1_structure and subset B with imp0_structure.
    # One solution for that woul be, in subset B, swap the positions (index) of defect atoms in the respective structure.

    even_match = False
    odd_match = False

    imp0_imp0s_match = Z_table['imp0'].equals(Z_table['imp0_structure'])
    imp1_imp1s_match = Z_table['imp1'].equals(Z_table['imp1_structure'])

    even_match = imp0_imp0s_match and imp1_imp1s_match

    if not even_match:
        imp0_imp1s_match = Z_table['imp0'].equals(Z_table['imp1_structure'])
        imp1_imp0s_match = Z_table['imp1'].equals(Z_table['imp0_structure'])

        odd_match = imp0_imp1s_match and imp1_imp0s_match

    if not (even_match or odd_match):
        raise NotImplementedError("Complex matching of Rcut columns to respective impurity atoms required. "
                                  "Only simple matching cases implemented.")

    # rename columns accordingly
    oldname_newname = {
        'Rcut_unit': 'KKR_rcut_unit',
        rcut_column_names[0]: 'KKR_rcut0' if even_match else 'KKR_rcut1',
        rcut_column_names[1]: 'KKR_rcut1' if even_match else 'KKR_rcut0'
    }
    table.rename(columns=oldname_newname, inplace=True)

    new_rcut_column_names = ['KKR_rcut0', 'KKR_rcut1', 'KKR_rcut_unit']
    return new_rcut_column_names


def load_jij_data(filepath: _typing.Union[str, _Path],
                  filename_table: str,
                  filename_structures: str,
                  silent: bool = False) -> _pd.DataFrame:
    """Load data of Jij isotropic exchange interaction from master thesis of Rubel Mozumder.

    Expects two data files in the same folder. `filename_table`: A pandas DataFrame. `filename_structures`: An
    `.extxyz` file, as specified in https://wiki.fysik.dtu.dk/ase/ase/io/formatoptions.html#extxyz.

    Combines both file sources into a single table, with column `'jij_uuid'` serving as primary key. This is
    the UUID from the original AiiDA dataset, specifically the UUIDs of the Jij `ArrayData` output nodes of the
    `kkr_imp_combine` workchains. Each workchain corresponds to the calculation of an impurity embedding of two
    transition metal defect atoms into the topological insulator Bi2Te3 as host crystal.

    The structures are the extracted combined impurity clusters around both impurities.

    Original data source: https://iffgit.fz-juelich.de/phd-project-wasmer/projects/mozumders-dimer-database.

    The original data was preprocessed to an intermediate format to be consumed by this function. This preprocessing
    is stored here: https://iffgit.fz-juelich.de/phd-project-wasmer/projects/mozumders-dimer-database/-/blob/master
    /notebooks/inspect_mozumders_ML_notebooks/search_db_for_ML_inputs.ipynb.

    :param filepath: Directory where the `.csv` and `.extxyz` input files reside.
    :param filename_table: The `.csv` pandas table.
    :param filename_structures: The `.extxyz` list of input structures.
    :param silent: True: Do not print progress.
    :return: DataFrame with all data from input files merged.
    """
    # read data from files
    filepath = _Path(filepath) if isinstance(filepath, str) else filepath
    if not filepath.is_dir():
        raise IOError(f"Filepath {filepath} is not a directory.")

    table = _pd.read_csv(filepath / filename_table)
    structures = _ase.io.read(filepath / filename_structures, index=':', format='extxyz')

    if not silent:
        print(f"Loaded Number samples: {len(structures)}, {table.shape[0]}.")

    # turn structures list into pandas table using their .info dicts
    structures_table = [struc.info for struc in structures]
    for info, structure in zip(structures_table, structures):
        structure.info = {}
        info['structure'] = structure

    structures_table = _pd.DataFrame.from_dict(structures_table)

    # merge both tables on column for jij uuids
    table = _pd.merge(structures_table, table,
                      left_on='jij_uuid', right_on='J_data_uuid',
                      how='left').drop('J_data_uuid', axis=1)

    # remove useless / redundant columns
    table.drop(columns=['workchain_uuid', 'workchain_ctime', 'calc_imps', 'Z_i', 'Z_j', 'i', 'j'],
               inplace=True)

    # reorder dynamically named columns

    # KKR Rcut columns processing / renaming
    # get Rcut column names
    rcut_column_names = [col for col in table.columns if col.startswith('Rcut_at_site_')]
    if not rcut_column_names:
        print("Warning: Found no 'Rcut_at_site_X' columns. Aborting Rcut processing.")
        # abort Rcut processing?

    if rcut_column_names:
        new_rcut_column_names = _process_rcut_columns(table=table,
                                                      rcut_column_names=rcut_column_names)

    # reorder static columns
    reordered_columns = ['workchain_label',
                         'imp0', 'imp1', 'ilayer0', 'ilayer1', 'offset0', 'offset1',
                         'structure',
                         'J', 'D', 'Dx', 'Dy', 'Dz',
                         'rx', 'ry', 'rz', 'r',
                         'mom1', 'mom2', 'tot_mom',
                         'jij_uuid']
    if rcut_column_names:
        # include renamed rcut columns in reordering, inbetween 'J' and 'rx' groups.
        reordered_columns[13:13] = new_rcut_column_names
    # reorder
    table = table[reordered_columns]

    return table
